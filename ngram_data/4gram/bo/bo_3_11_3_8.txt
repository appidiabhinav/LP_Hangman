both professional and personal M 13546
both instructors and students M 7602
both individuals and families M 3004
both construction and operation M 2224
both unnecessary and dangerous M 1924
both professional and academic M 1284
both individuals and business M 1207
both individuals and cultures M 868
both opportunity and challenge M 710
both troublesome and dangerous M 708
both unnecessary and improper M 658
both appreciative and critical M 640
both ineffective and dangerous M 586
both sequentially and randomly M 538
both adolescence and adulthood M 470
both explanations are possible M 458
both enhancement and depletion M 434
both professional and business M 423
both practically and ethically M 418
both development and evolution M 396
both governments and citizens M 388
both explanations are probably M 388
both professional and otherwise M 378
both governments and business M 370
both governments and industry M 354
both inhabitants and visitors M 340
both development and security M 313
both progressive and circular M 312
both interpreted and compiled M 312
both predictable and surprising M 298
both performance and security M 296
both instructive and agreeable M 296
both individuals and agencies M 296
both compensated and rewarded M 294
both institutions and students M 286
both significant and positive M 280
both individuals and churches M 272
both descriptions are accurate M 272
both intelligent and beautiful M 246
both understanding and practice M 245
both professional and cultural M 243
both inefficient and dangerous M 242
both unreasonable and dangerous M 240
both individuals and entities M 236
both technically and ethically M 234
both aesthetically and ethically M 234
both subconscious and conscious M 232
both unnecessary and wasteful M 218
both conceivable and possible M 216
both significant and negative M 205
both performance and potential M 203
both accompanied and followed M 200
both reproduction and survival M 198
both intelligent and agreeable M 198
both professional and auxiliary M 194
both maintenance and operation M 194
both fundamental and advanced M 190
both graphically and verbally M 188
both inefficient and wasteful M 182
both unnecessary and unlikely M 180
both adolescents and children M 176
both established and potential M 174
both informative and critical M 170
both personality and situation M 166
both professional and emotional M 164
both development and operation M 162
both achievement and attitude M 160
both understanding and interest M 158
both fundamental for purposes M 156
both experienced and observed M 154
both established and emergent M 153
both indifferent and ignorant M 152
both professional and amateurs M 150
both satisfaction and security M 144
both instructors and trainees M 144
both complicated and dangerous M 144
both distributed and retained M 142
both individuals and cultural M 140
both established and developing M 140
both mischievous and dangerous M 138
both embarrassed and relieved M 137
both complicated and enriched M 137
both intuitively and formally M 134
both individuals and property M 132
both independent and adequate M 132
both understanding and explaining M 130
both intelligent and educated M 130
both independent and impartial M 130
both safeguarded and reformed M 129
both technically and visually M 126
both particulars and generals M 126
both development and survival M 126
both competitors and potential M 126
both embarrassed and confused M 122
both generational and cultural M 121
both significant and relevant M 120
both instruments are designed M 120
both sequentially and directly M 118
both individuals and humanity M 118
both disappointing and surprising M 118
both development and execution M 118
both consistency and accuracy M 118
both professional and everyday M 116
both instructive and valuable M 116
both performance and attitude M 114
both unreasonable and contrary M 112
both unnecessary and contrary M 112
both opportunity and security M 112
both unnecessary and unwanted M 110
both instruments are equipped M 110
both ineffectual and dangerous M 108
both individuals and industry M 108
both governments and subjects M 108
both fundamental and essential M 108
both experienced and imagined M 108
both performance and audience M 107
both incremental and absolute M 106
both spiritually and otherwise M 104
both intelligent and ignorant M 104
both inhabitants and strangers M 104
both understanding and predicting M 100
both permissible and possible M 100
both advantageous and possible M 100
both professional and parental M 98
both understanding and expressing M 96
both persistence and patience M 96
both governments and churches M 96
both significant and valuable M 94
both performance and capacity M 94
both spiritually and carnally M 90
both informative and challenging M 90
both informative and accurate M 90
both significant and surprising M 88
both magnificent and terrible M 88
both intelligent and cultured M 88
both credibility and authority M 88
both understanding and judgment M 86
both understanding and evaluating M 86
both technically and formally M 86
both governments and governed M 86
both fundamental and specific M 86
both accelerated and straight M 86
both performance and personal M 84
both understanding and treatment M 82
both terminology and concepts M 82
both personality and attitude M 82
both advantageous and dangerous M 82
both significant and symbolic M 80
both intelligent and generous M 80
both experienced and possible M 80
both independent and assisted M 63
both progressive and backward M 47
both protestants and catholics D 21188
both agriculture and industry D 19694
both temperature and pressure D 12110
both transmitter and receiver D 8730
both affirmative and negative D 6341
both temperature and humidity D 5784
both transmission and reception D 4424
both manufacturing and services D 3440
both temperature and salinity D 2945
both prehistoric and historic D 2760
both temperature and moisture D 2211
both longitudinal and circular D 1958
both temperature and rainfall D 1926
both nonspecific and specific D 1908
both participant and observer D 1889
both preclinical and clinical D 1822
both demographic and economic D 1504
both geographical and cultural D 1454
both mathematical and physical D 1428
both agriculture and commerce D 1318
both thematically and formally D 1288
both necessities and luxuries D 1259
both hemispheres are involved D 1206
both temperature and relative D 1182
both anthropology and sociology D 1173
both humanitarian and economic D 1076
both environment and heredity D 1030
both capitalists and laborers D 1030
both prophylaxis and treatment D 982
both nationalists and unionists D 971
both agriculture and forestry D 959
both substantive and symbolic D 910
both professional and clerical D 883
both commodities and services D 836
both grammatical and semantic D 779
both transmitted and received D 762
both biographical and critical D 762
both recruitment and retention D 730
both differential and integral D 706
both noninvasive and invasive D 683
both descriptive and analytic D 675
both descriptive and critical D 636
both geographical and temporal D 626
both sociological and economic D 592
both generalized and specific D 582
both disagreeable and dangerous D 576
both commodities are produced D 572
both professional and consumer D 570
both theoretically and actually D 564
both statistical and clinical D 563
both translations and original D 536
both strategical and tactical D 536
both chimpanzees and gorillas D 531
both continental and maritime D 528
both professional and domestic D 516
both sedimentary and volcanic D 494
both probationers and parolees D 494
both intermediate and advanced D 491
both professional and financial D 490
both coefficients are positive D 468
both temperature and velocity D 460
both absenteeism and turnover D 460
both workmanship and materials D 452
both merchandise and services D 450
both practitioner and academic D 444
both necessaries and luxuries D 438
both competitive and monopoly D 432
both nonsurgical and surgical D 426
both politically and ethically D 424
both misdemeanors and felonies D 416
both impractical and dangerous D 402
both geographical and economic D 396
both spiritually and mentally D 386
both politically and sexually D 386
both manufacturing and commerce D 378
both commissioned and enlisted D 364
both synchronous and induction D 354
both subroutines and functions D 354
both functionally and spatially D 352
both conventional and criminal D 349
both advertisers and agencies D 346
both documentary and fictional D 344
both occupational and physical D 338
both conventional and advanced D 330
both prospective and practicing D 328
both functionally and visually D 328
both translations are possible D 326
both politically and otherwise D 324
both psychosocial and physical D 323
both established and dissenting D 323
both missionaries and converts D 318
both statistical and economic D 314
both transistors are conducting D 312
both instinctive and acquired D 310
both circumcised and baptized D 310
both disgraceful and dangerous D 308
both latitudinal and vertical D 306
both installation and operation D 306
both protagonist and narrator D 302
both eigenvalues are negative D 302
both fundamental and harmonic D 298
both assimilation and contrast D 298
both philosophic and religious D 294
both unnecessary and injurious D 292
both longitudinal and vertical D 292
both preventable and treatable D 287
both occupational and personal D 287
both nonclinical and clinical D 278
both pathological and clinical D 269
both maladaptive and adaptive D 269
both subordinate and dominant D 268
both biographical and literary D 266
both facilitated and hindered D 264
both intermediate and ultimate D 262
boy inaugurated his patriotic D 258
both comparative and absolute D 254
both affirmative and critical D 250
both scholarship and practice D 248
both professional and informal D 248
both conventional and tactical D 248
both journalists and scholars D 246
both missionaries and nationals D 244
both conservation and economic D 244
both undesirable and dangerous D 240
both understanding and retention D 238
both translations and rotations D 238
both convenience and accuracy D 236
both demographic and cultural D 235
both professional and economic D 234
both legislative and financial D 234
both friendships and romantic D 234
both agriculture and domestic D 234
both fundamental and clinical D 231
both proportional and absolute D 228
both cytoplasmic and membrane D 228
both geopolitical and economic D 224
both entertained and informed D 222
both materialists and idealists D 218
both storyteller and audience D 216
both environment and genetics D 212
both storyteller and listener D 210
both humanitarian and military D 210
both conventional and ultimate D 210
both entertained and educated D 208
both independent and mutually D 206
both coordinates are positive D 206
both recruitment and promotion D 204
both coordinates are negative D 204
both agriculture and business D 204
both depositional and erosional D 199
both temperature and magnetic D 198
both development and delivery D 196
both agriculture and medicine D 196
both achievement and aptitude D 195
both imaginative and realistic D 194
both proportional and integral D 193
both temporarily and spatially D 192
both superfluous and dangerous D 192
both spiritually and sexually D 192
both recruitment and selection D 192
both hemispheres are affected D 192
both amortization and interest D 191
both illumination and detection D 187
both conventional and distance D 187
both handicapped and otherwise D 186
both eigenvalues are positive D 186
both agriculture and services D 186
both precipitate and filtrate D 185
both masochistic and sadistic D 184
both flexibility and rigidity D 184
both evangelicals and catholics D 184
both magistrates and deputies D 182
both functionally and formally D 182
both longitudinal and torsional D 181
both nonmetallic and metallic D 179
both nonstandard and standard D 178
both fragmentary and complete D 178
both theologically and ethically D 176
both subtractive and additive D 176
both sensitivity and precision D 176
both intelligent and ambitious D 176
both differential and absolute D 176
both anesthetized and conscious D 176
both acceleration and velocity D 176
both scholarship and politics D 174
both geographical and emotional D 174
both environment and organism D 174
both temperature and wavelength D 173
both merchantmen and warships D 172
both legislature and judiciary D 172
both politically and fiscally D 171
both accelerated and retarded D 171
both transmitted and incident D 170
both sentimental and economic D 170
both nonreligious and religious D 170
both manufacturing and financial D 170
both documentary and literary D 168
both numerically and otherwise D 166
both subtropical and tropical D 165
both sociological and linguistic D 165
both substantive and editorial D 164
both intermediate and finished D 164
both flexibility and security D 164
both comfortable and beautiful D 162
both replacement and expansion D 161
both disagreeable and mortifying D 161
both scholarship and athletics D 160
both manufacturing and engineering D 158
both manufacturing and business D 158
both microscopic and chemical D 157
both criminology and criminal D 156
both accountants and managers D 156
both convenience and security D 155
both substantive and tactical D 154
both coefficients are negative D 154
both magnificent and terrifying D 153
both supervisors and students D 152
both supervisors and managers D 152
both practicable and eligible D 152
both assimilation and cultural D 152
both distasteful and dangerous D 150
both agriculture and education D 150
both comfortable and comforting D 149
both selfishness and altruism D 148
both numerically and visually D 148
both conventional and atypical D 148
both complicates and enriches D 148
both underground and opencast D 147
both sedimentary and basement D 146
both conventional and otherwise D 146
both contrivance and contriving D 146
both temperature and chemical D 144
both symptomatic and symbolic D 144
both organization and operation D 144
both monasteries and convents D 144
both legislators and citizens D 144
both institutions and policies D 144
both affirmations and negations D 144
both syntactical and semantic D 142
both inhabitants and soldiers D 142
both imaginative and critical D 142
both compressors and turbines D 142
both conventional and holistic D 141
both professional and literary D 140
both preliminary and detailed D 140
both nonfictional and fictional D 138
both continental and analytic D 138
both comestibles being obtained D 138
both colonization and conquest D 138
both proprietary and contract D 136
both picturesque and beautiful D 136
both evangelicals and liberals D 136
both electricity and hydrogen D 136
both hypertension and coronary D 134
both complicated and delicate D 134
both compensation and expenses D 134
both celebratory and critical D 134
both perestroika and glasnost D 133
both professional and romantic D 132
both philosopher and historian D 132
both nonmilitary and military D 132
both eligibility and benefits D 132
both manufacturing and purchasing D 131
both technically and mentally D 130
both conventional and computer D 130
both sensitivity and accuracy D 128
both occupational and domestic D 128
both conventional and modified D 128
both apprehension and docility D 128
both reactionaries and radicals D 127
both sedimentary and eruptive D 126
both nonacademic and academic D 126
both integrative and divisive D 126
both geographical and personal D 125
both susceptible and tolerant D 124
both impractical and unethical D 124
both celebrities and ordinary D 124
both protagonist and audience D 123
both broadsheets and tabloids D 123
both proprietary and personal D 122
both probability and severity D 122
both illuminates and obscures D 122
both menstruation and ovulation D 120
both inexpedient and contrary D 120
both battleships and cruisers D 118
both accumulation and inversion D 118
both sociological and religious D 117
both sociological and literary D 116
both sensitivity and response D 116
both numerically and spatially D 116
both mathematical and linguistic D 116
both manufacturing and materials D 116
both mammography and physical D 116
both longitudinal and tangential D 116
both individuals and financial D 116
both fibroblasts and epithelial D 116
both nonresident and resident D 115
both thematically and visually D 114
both salesperson and customer D 114
both illuminated and defended D 114
both corporations and counties D 114
both subcutaneous and visceral D 113
both intermediate and terminal D 113
both scholarship and activism D 112
both geographical and sectoral D 112
both friendships and enmities D 112
both electricity and gasoline D 112
both appointment and promotion D 112
both shareholder and employee D 110
both photographs and sketches D 110
both missionaries and officials D 110
both manufacturing and assembly D 110
both humanitarian and financial D 110
both flexibility and precision D 110
both demographic and clinical D 110
both conventional and chemical D 110
both transferred and reserved D 109
both evolutionary and cultural D 108
both documentary and physical D 108
both communities and families D 108
both supervisors and trainees D 106
both philosopher and novelist D 106
both manufacture and operation D 106
both legislative and judiciary D 106
both delinquents and controls D 106
both temperature and specific D 104
both professional and friendly D 104
both conveyances being pastured D 104
both conventional and critical D 104
both conservation and recreation D 104
both compositors and pressmen D 104
both agriculture and textiles D 104
both presupposes and produces D 102
both practicable and possible D 102
both percentages and absolute D 102
both programmers and managers D 100
both instructions and operands D 100
both preliterate and literate D 98
both inexpedient and dangerous D 98
both ethnographic and linguistic D 98
both conventional and original D 98
both symptomatic and specific D 96
both statistical and physical D 96
both scholarship and education D 96
both schismatics and heretics D 96
both permeability and porosity D 96
both motivational and emotional D 96
both intelligent and talented D 96
both controversy and confusion D 96
both contractors and engineers D 96
both automobiles and aircraft D 96
both adventurers and planters D 96
both temperature and absolute D 94
both nourishment and medicine D 94
both legitimizes and subverts D 94
both instructive and romantic D 94
both explanatory and critical D 94
both experiments and computer D 94
both disembodied and embodied D 94
both preliminary and advanced D 93
both shareholder and director D 92
both sentimental and humorous D 92
both restrictive and liberating D 92
both circumventing and satirizing D 92
both performance and financial D 90
both performance and economic D 90
both organization and employee D 90
both facilitates and inhibits D 90
both commendation and imitation D 90
both geographical and linguistic D 89
both saprophytes and pathogens D 88
both peristalsis and secretion D 88
both metaphysics and morality D 88
both documentary and dramatic D 88
both businessmen and tourists D 88
both unwarranted and dangerous D 86
both magistrates and citizens D 86
both extremities are involved D 86
both astronomers and civilians D 86
both topographic and climatic D 84
both substantive and emotional D 84
both speculators and settlers D 84
both journalists and citizens D 84
both intelligent and friendly D 84
both humanitarian and business D 84
both enlightened and patriotic D 84
both disagreeable and injurious D 84
both reductionist and holistic D 82
both professional and feminine D 82
both personality and physique D 82
both organization and doctrine D 82
both neoclassical and romantic D 82
both impertinent and improper D 82
both facilitated and hampered D 82
both experiential and didactic D 82
both acquisitions and internal D 82
both practicable and economic D 80
both explanations are partially D 80
both inventories and accounts D 62
both aristocrats and bourgeois D 62
both acquisitions and cataloging D 53
both illuminated and obscured D 51
both preparatory and terminal D 49
both proprietary and standard D 46
both fundamental and overtone D 46
both geographical and financial D 45
both documentary and personal D 41
