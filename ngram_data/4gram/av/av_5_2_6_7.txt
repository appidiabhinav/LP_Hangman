avoid the common mistake M 2371
avoid the choice between M 866
avoid the damaging effects M 864
avoid the common problem M 670
avoid or reduce anxiety M 330
avoid the latter problem M 318
avoid the tedious process M 316
avoid the entire problem M 262
avoid the almost certain M 232
avoid the costly process M 230
avoid or defend against M 220
avoid the second problem M 208
avoid the direct contact M 198
avoid an almost certain M 196
avoid the direct question M 190
avoid the crucial question M 188
avenge the injury offered M 186
avoid the common fallacy M 184
avoid the obvious problem M 164
avoid the dangers attending M 160
avoid the obvious question M 154
avoid the costly mistake M 154
avail so little against M 154
avoid the various diseases M 150
avoid or reduce damages M 142
avoid it simply because M 142
avoid the serious problem M 140
avoid the number thirteen M 138
avoid the losses resulting M 138
avoid the double payment M 130
avoid the reaction between M 128
avoid the public schools M 128
avoid the entire subject M 126
avoid the tragic results M 120
avoid an annual deficit M 116
avoid the ethical problem M 110
avoid the further chances M 106
avoid the entire process M 102
avoid the utmost penalty M 100
avoid the dangers threatening M 96
avoid the serious mistake M 94
avoid the larger question M 92
avoid the direct effects M 90
avoid the serious effects M 88
avoid the charge against M 86
avenge the crimes against M 86
avoid the duties imposed M 84
avoid the direct control M 84
avoid the curious glances M 84
avoid by common consent M 84
avenge the slight offered M 44
avenge the insult offered D 2784
avoid the double taxation D 2718
avoid the common pitfall D 778
avenge an insult offered D 684
avoid the supine position D 562
avert the dangers threatening D 518
avenging the insult offered D 506
avoid the trivial solution D 458
avoid the tension between D 294
avoid the double jeopardy D 286
avoid the further effusion D 262
avoid the public scandal D 226
avoid an oncoming vehicle D 192
avoid the greater expense D 180
avenging an insult offered D 170
avoid the ethical dilemma D 160
avoid the narrow streets D 156
avert the divine vengeance D 152
avoid the tangled forests D 148
avoid the policy because D 148
avoid the duties payable D 148
avoid the arrest warrant D 144
avenge the impious mockery D 124
avoid the obvious pitfall D 118
avoid the roller coaster D 116
avoid the oncoming traffic D 114
avoid the stormy passage D 106
avoid the public streets D 104
avoid the stifling effects D 100
avert the various plagues D 100
avenge the unceasing insults D 100
avert the horrid cruelty D 96
avoid the sticky problem D 92
avoid the strait guarded D 90
