an elite without successors M 270
an order granting appropriate M 232
an offer before acceptance M 216
an order awarding appropriate M 202
an exact binary equivalent M 189
an irony without bitterness M 174
an inner circle surrounded M 162
an equal degree apparently M 158
an often quoted description M 148
an event highly improbable M 134
an agent without sufficient M 116
an event highly unexpected M 110
an appeal through experience M 106
an added factor contributing M 104
an older person experiencing M 102
an issue already determined M 102
an appeal beyond boundaries M 102
an exact verbal equivalent M 98
an exact modern equivalent M 96
an event wholly unexpected M 94
an equal degree multiplied M 92
an actor should understand M 92
an exact verbal description M 84
an event without historical M 82
an event almost impossible M 80
an equal amount constantly M 80
an upper airway obstruction D 1650
an urban police department D 1024
an issuing public corporation D 884
an adult female chimpanzee D 500
an extra credit assignment D 462
an index number representing D 354
an equal number representing D 284
an equal dollar investment D 256
an acute attack supervenes D 250
an action alleging negligence D 246
an urban public university D 228
an action almost unexampled D 228
an author without incongruity D 196
an extra female chromosome D 182
an action already instituted D 176
an abbot without discipline D 174
an enemy kindle resentment D 168
an acute airway obstruction D 165
an urban design competition D 160
an asset through depreciation D 158
an utter mutual confluence D 156
an olive garden restaurant D 150
an inert gaseous atmosphere D 132
an azure ladder transverse D 130
an order before prescribed D 124
an action alleging infringement D 118
an order granting defendants D 110
an extra marker chromosome D 110
an index number calculated D 106
an order granting plaintiffs D 96
an urban planning consultant D 94
an actor turned playwright D 94
an index figure representing D 92
an inert helium atmosphere D 90
an added series resistance D 90
an error signal representing D 84
an arrow diagram representing D 82
