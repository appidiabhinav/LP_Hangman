his enemies round about M 2775
him several times since M 2528
him several times about M 2184
him several times after M 1882
his parents still lived M 1692
his wildest dreams could M 1394
his counsel shall stand M 1324
his decision thereon shall M 1214
his prayers every night M 1192
him several times while M 1160
him several months later M 1033
his friends could never M 1002
his article cited above M 973
his majesty shall please M 964
his general views about M 902
his majesty could never M 802
his parents while still M 760
his regular working hours M 752
his eternal resting place M 746
his decision until after M 670
his opinions about women M 644
his conduct might appear M 622
him several hours later M 607
him several weeks later M 604
his parents could never M 590
his leisure hours within M 542
him seventy times seven M 518
his majesty shall judge M 512
his glorious grace which M 492
his country could never M 488
him several times looking M 478
his enemies could never M 474
him against every enemy M 472
his trembling hands could M 468
his peculiar views about M 464
his promise never again M 452
him several times again M 452
him several times until M 436
his prayers every morning M 430
his position until after M 422
his private house wrote M 410
his advance guard under M 410
his remarks about women M 402
his majesty shall cause M 378
his parents until after M 364
his conduct which might M 362
him several months after M 358
his emotions under tight M 352
his friends stood round M 346
his counsel shall abuse M 334
his jealousy strong about M 326
his command until after M 326
his majesty asked which M 324
his picture taken while M 322
his beliefs about human M 308
his country could offer M 306
his absence which could M 304
his country house about M 298
his closest white house M 296
his majesty rather asked M 294
his position until night M 292
his absence every morning M 292
his worries about money M 288
his general point about M 288
his parents still alive M 282
his friends could easily M 282
him several times every M 278
his friends asked where M 276
his balance while walking M 276
his attacks above sixty M 276
his anxiety about money M 275
him several times later M 274
his friends could enjoy M 270
his essence could admit M 268
his conduct still right M 268
his question about where M 262
his general feeling about M 262
his present state could M 260
his general abode shall M 260
his thoughts about women M 256
his friends gather round M 254
his current thinking about M 254
his friends every evening M 252
his efforts might prove M 252
his greatest works after M 248
his greatest enemy could M 246
his conduct which really M 246
his mistake until after M 244
his studies while working M 240
his decision about which M 238
his beliefs about women M 234
his majesty might issue M 230
his leisure hours writing M 230
his majesty shall order M 228
his country while still M 228
his arrival until after M 228
his friends round about M 226
his conduct while under M 226
his teachers never could M 224
his harness alone saved M 224
his remaining quiet until M 222
him several times round M 221
his private views about M 220
his country house where M 218
his thoughts about human M 216
his marriage until after M 214
his enemies never ceased M 214
his conduct which could M 206
his minimum selling price M 204
his country which could M 204
his majesty might judge M 202
him several times under M 202
his southern kingdom could M 200
his friends often found M 198
his friends might enjoy M 198
his concern about getting M 198
his college broke loose M 198
him several weeks after M 198
his adopted child seated M 196
him several times daily M 194
his thoughts turning again M 192
his position could never M 192
his illness again quite M 192
his friends stood about M 192
his counsel shall appear M 192
his friends never ceased M 190
his friends might amuse M 190
his conduct might cause M 190
him several hours after M 188
his majesty shall allow M 186
his faithful agent again M 186
his thoughts still dwelt M 184
his private inner world M 184
his present happy state M 184
his justice might claim M 184
his friends might prove M 184
his failure about thirty M 184
his decision about where M 184
his journey until after M 180
his beliefs about teaching M 180
his present resting place M 178
his parents arguing about M 178
his western white house M 176
his thoughts still running M 174
his stories about dreams M 174
his service every shape M 174
his precious blood which M 174
his wounded pride either M 172
his decision within thirty M 172
his history which might M 170
his command shall build M 170
his triumph until about M 168
his purpose never again M 168
his boasted reason seems M 168
his academy award winning M 168
his natural speaking voice M 167
his liberty until after M 166
his friends could spare M 164
his friends could raise M 164
his address which belong M 164
his patient every hunting M 162
his parents until about M 162
his friends could visit M 162
his decision every action M 162
his central claim about M 162
him several times below M 159
his supreme power which M 158
his parents later moved M 158
his leisure hours playing M 158
his country could boast M 158
his command under cover M 158
his majesty might easily M 156
his friends began again M 156
his general thinking about M 154
his friends whose names M 154
his freedom until after M 154
his picture taken holding M 152
his opinions about music M 152
his concern about money M 152
him bravely while alive M 152
his wishful thinking about M 150
his unaided might saved M 150
his greatest state paper M 150
his friends could still M 150
his country after nearly M 150
his ambition could never M 150
his remaining longer might M 148
his parents argue about M 148
his friends might never M 148
his assumed moral sense M 148
his unknown beauty might M 146
his leisure hours while M 146
his friends every night M 146
his concern about human M 146
his bravery could never M 146
his usually quiet voice M 144
his heavenly father could M 144
his central point about M 144
his marriage falling apart M 142
his friends gather about M 142
his country people lived M 142
his college either lasting M 142
his absence might cause M 142
his thoughts still fixed M 140
his intense feeling about M 140
his country after world M 140
his present sorry state M 138
his natural voice since M 138
his leisure hours after M 138
his thoughts about writing M 136
his subject rises proud M 136
his majesty still seeks M 136
his friends could stand M 136
his concern about keeping M 136
his majesty could still M 134
his beliefs about people M 134
him several times within M 134
him against every danger M 134
his studies until after M 132
his penance might avail M 132
his parents again after M 132
his opinions about anything M 132
his friends could gather M 132
his efforts could never M 132
him promise never again M 132
his thoughts about music M 130
his success where others M 130
his prayers every evening M 130
his position quite plain M 130
his parents lived until M 130
his opinions about people M 130
his majesty sitting under M 130
his greatest works while M 130
his enemies under cover M 130
his enemies search every M 130
his dignity which makes M 130
his country which might M 130
his brothers still lived M 130
his shorter prose works M 129
his vessels under cover M 128
his thoughts about poetry M 128
his miracle working power M 128
his enemies might still M 128
his country nurse might M 128
his regular resting hours M 126
his lecture notes which M 126
his history eight times M 126
his friends found fault M 126
his decision which shall M 126
him several times today M 125
his results until after M 124
his friends until after M 124
his friends could offer M 124
his equally strong sense M 124
him certain facts which M 124
his parents worry about M 123
his majesty first hides M 122
his friends might still M 122
his enraged enemy might M 122
his country house today M 122
him forever uneasy under M 122
his written paper which M 120
his friends began calling M 120
his emotions under wraps M 120
his designs crave haste M 120
his country house which M 120
his greatest short story M 118
his company could easily M 118
his trembling hands above M 116
his private world which M 116
his opinions about human M 116
his majesty might order M 116
his highest total since M 116
his freedom after seven M 116
his country might never M 116
his weakness could stand M 114
his parents might still M 114
his parents again until M 114
his friends still hoped M 114
his friends every where M 114
his conduct comes under M 114
his anxiety about getting M 114
his wildest dreams never M 112
his superior brain power M 112
his parents while growing M 112
his parents never really M 112
his parents moved after M 112
his majesty shall grant M 112
his diseased state turns M 112
his conduct under given M 112
him succeed where others M 112
his purpose which could M 110
his position might appear M 110
his material needs alone M 110
his illness until after M 110
his illness might prove M 110
his heavenly father which M 110
his hardest trials still M 110
his friends spent hours M 110
his friends could watch M 110
his counsel which might M 110
his average total costs M 110
his average selling price M 110
him stories about people M 110
his repeated phone calls M 109
his buildings still stand M 109
his balance while sitting M 109
his selfish views under M 108
his purpose while really M 108
his precious wares about M 108
his friends could catch M 108
his freedom after winning M 108
his command which could M 108
his command about seven M 108
his comfort shall never M 108
his thoughts found words M 106
his stories about people M 106
his imagery still appear M 106
his concern about people M 106
his company could build M 106
him certain facts about M 106
his thoughts about party M 104
his studies while teaching M 104
his remarks about poetry M 104
his natural moral sense M 104
his motives which arise M 104
his leisure hours under M 104
his enemies could easily M 104
his brothers might track M 104
him certain marks which M 104
his studies first began M 102
his purpose under words M 102
his private study where M 102
his private readings which M 102
his marriage takes place M 102
his letters under cover M 102
his journal entry about M 102
his friends might visit M 102
his friends again after M 102
his conduct which forms M 102
his brothers every morning M 102
his remarks about people M 100
his picture taken sitting M 100
his peculiar style either M 100
his parents cared about M 100
his letters still exist M 100
his friends asked about M 100
his counsel until after M 100
his company every morning M 100
his blessed mother given M 100
his beliefs about which M 100
his beliefs about poetry M 100
his thoughts about people M 98
his revolving chair right M 98
his natural strong sense M 98
his marriage could never M 98
his illness grows worse M 98
his example which moved M 98
his example might serve M 98
his advance badly beaten M 98
his teachers could never M 96
his parents never found M 96
his parents about where M 96
his natural father about M 96
his justice which proud M 96
his friends still lived M 96
his conduct stand alone M 96
his position under cover M 94
his position could easily M 94
his largest ships could M 94
his greatest music after M 94
his friends could claim M 94
his extreme views about M 94
his country could avoid M 94
his company could offer M 94
his anxiety about women M 94
his account books showing M 94
him several works which M 94
him everything which could M 94
his conduct under cross M 93
his regular morning visit M 92
his precious blood alone M 92
his parents really loved M 92
his natural poetic gifts M 92
his majesty began first M 92
his journal entry dated M 92
his holiest people might M 92
his general claim about M 92
his friends wrote about M 92
his friends never failing M 92
his friends might laugh M 92
his friends later wrote M 92
his country until after M 92
his country house after M 92
his concern about growing M 92
his ambition might cause M 92
his records still stand M 91
his success could never M 90
his studies every morning M 90
his southern blood makes M 90
his purpose shall stand M 90
his present state might M 90
his position might prove M 90
his position might easily M 90
his peculiar gifts could M 90
his parents could raise M 90
his opinions about poetry M 90
his friends could really M 90
his delight could never M 90
his country never lived M 90
his conduct while alive M 90
his command until within M 90
him several times playing M 90
him several hours since M 90
him confess every crime M 90
his trembling hands under M 88
his stories takes place M 88
his stories about women M 88
his parents every night M 88
his parents every morning M 88
his parents anything about M 88
his majesty never could M 88
his letters which might M 88
his jealousy alone which M 88
his general order after M 88
his friends could carry M 88
his decision never again M 88
his comfort which could M 88
him several times where M 88
his wounded pride could M 86
his private sense alone M 86
his parents still loved M 86
his majesty might enter M 86
his letters which refer M 86
his letters every where M 86
his example might never M 86
his creature keeping house M 86
him several times check M 86
his workers shall enjoy M 84
his position still worse M 84
his parents could offer M 84
his library every night M 84
his friends seems never M 84
his friends might easily M 84
his fellows faced about M 84
his country place where M 84
his correct drawing within M 84
his command alarm fires M 84
his subject which crowd M 82
his stories about growing M 82
his research while working M 82
his religion every where M 82
his passive mother could M 82
his parents asked about M 82
his natural reason alone M 82
his majesty every morning M 82
his example still lives M 82
his editors twice about M 82
his country above every M 82
his concern about water M 82
his subject which makes M 80
his picture every night M 80
his parents could sleep M 80
his parents could easily M 80
his offerings could never M 80
his language about women M 80
his friends stood guard M 80
his friends never again M 80
his decision could never M 80
his birthday comes round M 61
his regular phone calls M 45
his leisure hours either M 45
his position while still M 43
his pavilion round about D 2854
his majesty would never D 2264
his parents would never D 2234
his majesty would please D 1850
his friends would never D 1560
his majesty would grant D 1526
his country would never D 1456
his earliest known works D 1086
his jealousy shall smoke D 1074
his majesty would allow D 1048
his parents talking about D 944
his heavenly father would D 914
his wildest dreams would D 846
his majesty would order D 824
his parched tongue clove D 803
his nightly walks about D 774
his friends would laugh D 750
his mounted scale aloft D 744
his creative writing class D 710
his widowed mother lived D 700
his vantage point above D 694
his subject which would D 624
his friends would often D 618
his conduct would cause D 610
his company would often D 606
his enemies would never D 602
his friends talking about D 530
his friends would gather D 518
his exposed right flank D 511
his widowed mother moved D 506
his arterial blood gases D 502
his clasped hands above D 495
his election agent shall D 478
his friends would stand D 454
his position would allow D 438
his majesty would cause D 424
his widowed mother could D 420
his efforts would prove D 418
his company would never D 418
his clasped hands resting D 418
his country which would D 414
his widowed mother until D 408
his funeral takes place D 402
his brethren round about D 402
his dignity would allow D 398
his absence would cause D 398
his present state would D 390
his friends would visit D 384
his forehead would catch D 382
his general thesis about D 378
his bedroom every night D 370
his majesty would rather D 366
his friends would unite D 366
his official title rather D 362
his guarded throne could D 360
his majesty would aught D 358
his average daily earnings D 358
his friends would agree D 338
his position would still D 336
his library would never D 336
his country would fight D 332
his parents would allow D 322
his replies would alone D 316
his scarlet would blush D 312
his tenants could agree D 310
his horizon still flies D 308
his majesty would deign D 302
his brothers would never D 302
his classic short story D 300
his remarks would apply D 294
his average daily wages D 294
his position would never D 292
his chamber every night D 292
his bedside every night D 292
his majesty never would D 290
his friends would still D 286
his servant seven times D 278
his leisure would allow D 278
his conduct which would D 276
his ingenuity could grasp D 268
his affairs would admit D 268
his private staff shall D 262
his earliest dated works D 262
his central thesis about D 262
his features which spoke D 258
his command would allow D 258
his sidereal clock shall D 256
his deceased father would D 256
his conduct while rubbing D 256
his chamber music works D 254
his thoughts would often D 252
his diamonds pours apace D 252
his friends would share D 250
his slender white fingers D 248
his parents would rather D 246
his bedside every morning D 246
his private wrongs might D 244
his classic fairy tales D 243
his official white house D 242
his friends would spend D 242
his behavior would change D 240
his swagger stick under D 238
his steward might deign D 238
his resolve never again D 238
his majesty would enter D 238
his conduct falls within D 238
his warriors fully armed D 234
his extreme right flank D 233
his offense level under D 232
his concern about social D 230
his fertile brain could D 228
his country would stand D 228
his absence would create D 228
his majesty would still D 226
his friends would allow D 226
his friends would rather D 224
his fevered brain grows D 222
his earliest known dated D 222
his digital alarm clock D 222
his company would build D 222
his majesty would either D 221
his slender white hands D 218
his rumpled white shirt D 218
his position would appear D 218
his leisure hours would D 218
his illness would prove D 218
his forehead which shone D 218
his private minor loans D 216
his parents would still D 214
his anxiety about liege D 214
his widowed mother would D 210
his muscles tense beneath D 210
his liberal views about D 210
his eardrums would burst D 210
his profiting might appear D 206
his conduct would appear D 204
his talents would never D 202
his present grade level D 202
his popular action party D 202
his efforts never slack D 200
him started talking about D 198
his popular plays alike D 196
his decision would cause D 196
his country would avenge D 196
his brethren would never D 196
his ruffled shirt front D 194
his pleasant tenor voice D 194
his majesty would march D 194
his majesty would easily D 194
his creative power could D 194
his trembling fingers would D 192
his arrival would cause D 192
his conduct would prove D 190
his parents would agree D 188
his friends which would D 188
his clasped hands under D 188
his motives would appear D 186
his gallant white horse D 186
his eyelids droop again D 186
his trembling limbs could D 184
his thoughts would drift D 184
his balance sheet would D 184
his trembling limbs would D 182
his subject crows gazed D 182
his muscles tense under D 182
his silvery tongue began D 180
his friends would enjoy D 180
his vantage point within D 178
his modesty would allow D 178
his liberal social views D 178
his critics would later D 178
his cassock would serve D 178
his article would appear D 178
his friends would start D 176
his widowed mother after D 174
his noblest peers under D 174
his friends would carry D 174
his friends could drink D 174
his enemies would seize D 174
his enemies would allow D 174
his trembling hands would D 172
his thoughts would recur D 172
his stomach heaved again D 172
his radical social views D 172
his natural gifts would D 172
his kitchen table while D 172
his features would relax D 172
his vantage point under D 170
his revenue which arose D 170
his friends would rally D 170
his beloved child could D 170
him several smart sayings D 170
his radical views about D 168
his primary social group D 168
his friends would later D 168
his dignity could brook D 168
his weakness would allow D 166
his pressed white shirt D 166
his eyelids close again D 166
his coaster would never D 166
his bedroom every morning D 166
his ascetic gloom boded D 166
his account books bound D 166
his rapidly changing world D 165
his thoughts would stray D 164
his picture would appear D 164
his overall grade point D 164
his talents would shine D 162
his stained white apron D 162
his slender brown hands D 162
his country again waved D 162
his comrade under cover D 162
his trembling fingers could D 160
his wounded pride would D 158
his friends would appear D 158
his musical mother tongue D 156
him eminent above others D 154
his support would never D 152
his secular vocal music D 152
his muscles bunch beneath D 152
his servant bending under D 150
his marriage falls apart D 150
his majesty would avoid D 150
his gnarled walking stick D 150
his balance sheet shows D 150
his widowed mother still D 148
his supreme court brief D 148
his superior naval force D 148
his history which would D 148
his grammar would write D 148
him resolve never again D 148
his official state visit D 146
his eyelids would droop D 146
his eyelids would close D 146
his friends joked about D 145
his official suite would D 144
his friends would enter D 144
his talents would allow D 142
his stomach could stand D 142
his ruffled white shirt D 142
his present state fanny D 142
his friends would drive D 142
his blasted limbs still D 142
his pockets every night D 140
his partner would never D 140
his curdled blood began D 140
his critics would admit D 140
his command which would D 140
his bombast until every D 140
his thoughts would dwell D 138
his language would imply D 138
his friends would trust D 138
his friends would drink D 138
his command would admit D 138
his classic novel about D 138
his request would appeal D 136
his precious knife again D 136
his majesty still talks D 136
his gallant black horse D 136
his friends would write D 136
his friends sinking round D 136
his election would prove D 136
his conduct falls short D 136
his muscles moved beneath D 134
his married women weeping D 134
his kinsmen could amend D 134
his forehead shone where D 134
his muscles bulging under D 130
his critics would argue D 130
his affairs would allow D 130
his widowed mother while D 128
his position would admit D 128
his greatest danger would D 128
his friends hoped would D 128
his friends feared would D 128
his election takes place D 128
his country would allow D 128
his youngest child would D 126
his thoughts would still D 126
his swiftly changing moods D 126
his quickly changing moods D 126
his private parking space D 126
his national front party D 126
his friends could rally D 126
his bedside either under D 126
his official power could D 124
his modesty would never D 124
his friends would reply D 124
his fertile fancy could D 124
his creative power would D 124
his command would never D 124
his average yearly earnings D 124
his written works would D 122
his sisters would never D 122
his message would never D 122
his massive leather chair D 122
his kinsmen would never D 122
his critics would agree D 122
his country would enter D 122
his company which would D 122
his conduct falls below D 121
his warriors would often D 120
his refusal would cause D 120
his letters which would D 120
his country waved aside D 120
his bicycle every morning D 120
his behavior comes under D 120
him splitting hairs about D 120
his workmen which comes D 118
his vantage point beneath D 118
his poetical works which D 118
his picture taken shaking D 118
his marriage would never D 118
his harness would allow D 118
his friends would offer D 118
his creative poetic fancy D 118
his courage would carry D 118
his bedside table where D 118
his arrival would spread D 118
his stories which would D 116
his stomach would burst D 116
his shapely white hands D 116
his servant lodge within D 116
his sermons could never D 116
his position would change D 116
his parents would often D 116
his natural state would D 116
his forlorn trees pined D 116
his faithful people would D 116
his cruelty shall never D 116
his clasped hands bulge D 116
his forlorn state wishing D 115
his stained white shirt D 114
his slender brown fingers D 114
his official working hours D 114
his friends would admit D 114
his deposit money shall D 114
his behavior while under D 114
his average daily sales D 114
his weakness spoke first D 112
his parents would visit D 112
his parents which would D 112
his nightly phone calls D 112
his natural baked color D 112
his majesty would agree D 112
his friends would watch D 112
his essence would sound D 112
his crooked walking stick D 112
his beloved mother would D 112
him excited about anything D 112
his petition either state D 111
his present abiding place D 110
his massive chest heaved D 110
his imperial father might D 110
his critics would allow D 110
his country morning dress D 110
his arrival within sight D 110
hit several times while D 109
his servant would never D 108
his rapidly changing moods D 108
his patient drugs which D 108
his opinions would carry D 108
his meridian glory looks D 108
his medical costs might D 108
his forward panting horse D 108
his enemies would later D 108
his enemies would admit D 108
his country would agree D 108
his convent would allow D 108
his clients would never D 108
his blanket drawn close D 108
his acutest vision would D 108
his stomach every morning D 106
his request would cause D 106
his lyrical poetry which D 106
his enemies would prove D 106
his decline would change D 106
his bungalow stood empty D 106
his behavior makes sense D 106
his virtues would shine D 104
his supreme court appeal D 104
his superior force would D 104
his majesty shook hands D 104
his earliest works about D 104
his cottage every night D 104
his comrade would await D 104
his brigade under cover D 104
his behavior takes place D 104
his bedside alarm clock D 104
him several sharp blows D 104
his thoughts working would D 102
his sparkling white shirt D 102
his pockets which could D 102
his muscles still ached D 102
his glasses still fixed D 102
his forehead would burst D 102
his eternal jewel given D 102
his current skill level D 102
his country seated aloft D 101
his worldly goods would D 100
his talents would carry D 100
his superior skill would D 100
his medical trials within D 100
his forehead seven times D 100
his decision would stand D 100
his courage never fails D 100
his boatmen still slept D 100
his banners again under D 100
his ambition would never D 100
his spirits sinking within D 98
his sermons stand apart D 98
his parents would drive D 98
his friends would smile D 98
his forehead which would D 98
his efforts would never D 98
his current grade level D 98
his country would again D 98
his account would appear D 98
his servant might carry D 96
his serried ranks shall D 96
his official title would D 96
his musical tenor voice D 96
his journey thither would D 96
his friends would engage D 96
his enemies would expel D 96
his earliest major works D 96
his creative power which D 96
his success would cause D 94
his subject would allow D 94
his product under guise D 94
his previous views about D 94
his precious blood would D 94
his popular comic strip D 94
his peculiar stone touch D 94
his message would still D 94
his manners would allow D 94
his majesty would first D 94
his liberal racial views D 94
his friends would grant D 94
his friends never tired D 94
his freedom after serving D 94
his courage would never D 94
his conduct while serving D 94
his captain shook hands D 94
his earliest prose works D 93
his present queer state D 92
his position which would D 92
his position inter pares D 92
his parents would worry D 92
his parched throat would D 92
his kindred world began D 92
his inferior would touch D 92
his illness would allow D 92
his hundred wives under D 92
his features rather small D 92
his example would serve D 92
his enemies never tired D 92
his destiny would close D 92
his crimson leather chair D 92
his courage stood every D 92
his collars never stiff D 92
his carriage while driving D 92
his bladder would burst D 92
his ability would carry D 92
his thoughts would begin D 90
his suicide would cause D 90
his rapidly growing power D 90
his popular books about D 90
his parents would spend D 90
his parents never spoke D 90
his parched throat about D 90
his orations still please D 90
his friends would build D 90
his friends playing cards D 90
his friends never would D 90
his exploring fingers found D 90
his enemies would spare D 90
his ancient rival might D 90
his success would carry D 88
his features which added D 88
him phantom human forms D 88
his trembling knees could D 86
his teachers would never D 86
his sermons about sheep D 86
his natural place would D 86
his national unity party D 86
his muscles tensing beneath D 86
his muscles flexing beneath D 86
his majesty would write D 86
his majesty would admit D 86
his loyalty would never D 86
his greatest enemy would D 86
his friends would arise D 86
his fortune would allow D 86
his fondest hopes would D 86
his dreadful cough would D 86
his country would adopt D 86
his brothers would laugh D 86
his arrival would create D 86
his absence which would D 86
his vacation under close D 84
his subject would admit D 84
his pursuer would smash D 84
his muscles moved under D 84
his mockery acted again D 84
his injured ankle would D 84
his imperial robes which D 84
his friends would prove D 84
his friends would fight D 84
his friends spoke about D 84
his earliest works while D 84
his dynasty ruled until D 84
his crooked smile again D 84
his concern about japan D 84
his closest aides would D 84
his bullets would never D 84
his blanket every night D 84
his betrothing takes place D 84
him certain metal coins D 84
his wildest freaks appear D 82
his thoughts would never D 82
his talents would appear D 82
his steamers moved first D 82
his results would appear D 82
his quarter fifty cents D 82
his pistols every night D 82
his parched tongue could D 82
his musical voice asked D 82
his friends would raise D 82
his flaunting court about D 82
his features would light D 82
his elderly mother would D 82
his dynasty would never D 82
his creative brain loved D 82
his cousins sitting about D 82
his conduct would create D 82
his company would spend D 82
his chamber twice every D 82
his behavior which would D 82
his written style halts D 80
his widowed mother lives D 80
his talents would prove D 80
his stomach would never D 80
his spirits would cause D 80
his private purse until D 80
his parents hoped would D 80
his parched tongue would D 80
his musical gifts would D 80
his majesty would value D 80
his majesty would carry D 80
his friends would begin D 80
his enemies would still D 80
his election would never D 80
his drunken boors dancing D 80
his critics would never D 80
his country while serving D 80
his country manor house D 80
his adopted father would D 80
him several times driving D 80
his friends would either D 51
his hickory walking stick D 45
his vantage point below D 43
