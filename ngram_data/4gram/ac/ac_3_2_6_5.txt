act to further amend M 6260
act of divine grace M 5820
act of divine power M 3396
act of creation which M 2374
act of thought which M 2014
act no person shall M 1543
act of another state M 1449
act to borrow money M 1374
act of becoming aware M 1268
act of homage which M 1200
act on advice given M 878
act of common sense M 876
act of choice which M 874
act of special grace M 870
act of public enemy M 850
act of existing which M 840
act in another state M 827
act of public force M 612
act of virtue which M 606
act of public speaking M 585
act is silent about M 566
act of mental vision M 550
act as helper cells M 488
act so passed shall M 438
act of speech which M 432
act is passed which M 422
act of creation rather M 418
act or advice which M 397
act by itself alone M 396
act of divine favor M 372
act or thought which M 369
act by another state M 356
act of letter writing M 352
act of murder which M 346
act of thought about M 342
act of creation takes M 334
act of twelfth night M 332
act of creation could M 330
act in common cases M 322
act as managing agent M 320
act in almost every M 319
act of nature which M 296
act to reside within M 294
act of creation seems M 290
act of memory which M 284
act of choosing which M 276
act in groups rather M 269
act as active sites M 268
act of another which M 260
act of another party M 260
act of greater power M 256
act of another human M 247
act on prices until M 246
act of modern times M 230
act of public power M 227
act or measure which M 222
act as market maker M 219
act in itself which M 218
act of honour which M 214
act of special merit M 206
act in various parts M 206
act of heroic folly M 201
act of standing still M 200
act is something which M 199
act of family piety M 195
act of deciding which M 194
act at various sites M 193
act of wanton folly M 190
act of donating blood M 189
act of standing erect M 183
act of prayer which M 180
act as sounding board M 180
act to confer civil M 179
act of sleeping beauty M 178
act of creation within M 178
act as something which M 177
act of creation might M 172
act of belief which M 172
act on global warming M 171
act of becoming human M 170
act of direct force M 164
act in unison under M 163
act an amount equal M 160
act of creation began M 156
act to enable joint M 155
act of wisdom which M 152
act of another power M 152
act in special cases M 152
act or object which M 146
act of imposing order M 146
act of special favor M 144
act of murder could M 144
act of creation comes M 144
act as killer cells M 143
act of praise which M 142
act on various parts M 141
act to effect change M 140
act of coercion which M 138
act of another agent M 138
act or speech which M 137
act of greater folly M 136
act of creation gives M 134
act by another cause M 134
act on common minds M 132
act or mental state M 130
act of second order M 130
act of reasoning which M 130
act of homage could M 130
act as special agent M 127
act is hereby amend M 126
act of special piety M 124
act as secret agent M 124
act of creation alone M 123
act as memory cells M 123
act of passion which M 122
act to reduce blood M 121
act the moment after M 120
act of thought whose M 120
act is judged right M 120
act in itself could M 120
act as agents which M 120
act in itself might M 118
act by another party M 118
act on immune cells M 117
act in modern times M 117
act or demand shall M 116
act of signal folly M 116
act of mutual fight M 116
act of gender based M 112
act at various times M 112
act at proper times M 112
act as agents within M 112
act to reduce risks M 110
act of murder seems M 110
act as source rocks M 108
act as guards while M 108
act of genius which M 106
act as special judge M 106
act as quoted above M 105
act the united kingdom M 104
act as teacher aides M 104
act as ground cover M 104
act the various parts M 102
act of favour which M 102
act of creation makes M 102
act of becoming solid M 102
act is passed shall M 102
act to reduce costs M 100
act of simple human M 100
act in urgent cases M 98
act on various types M 97
act to enable women M 96
act on emotion rather M 96
act of thought knowing M 96
act of creation where M 96
act of choice rather M 96
act in global terms M 96
act on dividing cells M 95
act of creation while M 94
act of donating money M 93
act of dropping strong M 92
act is better known M 92
act on reasons which M 90
act of nature could M 90
act of hubris which M 90
act on assuming power M 88
act of insane folly M 88
act of allowing light M 88
act by itself might M 88
act in common cause M 87
act as agents under M 87
act of mutual trust M 86
act of homage really M 86
act it without holding M 86
act of pardon which M 84
act is always right M 84
act as agents rather M 84
act on issues which M 82
act of murder might M 82
act of dropping beneath M 82
act of counting which M 82
act of choice takes M 82
act in purely local M 82
act in effect gives M 82
act as useful tools M 82
act of offering money M 80
act of larger fraud M 80
act of creation still M 80
act as standing alone M 80
act as speaker until M 80
act on almost every M 54
act as forces which M 50
act is clearly shown M 47
act as stated above M 45
act to retain water M 42
act in future cases M 41
act at various angles M 40
act of divine mercy D 2101
act as fiscal agent D 1816
act of sexual abuse D 1358
act on target cells D 1196
act as regent until D 1089
act of treason which D 749
act of creation would D 622
act as buffer zones D 595
act to render valid D 506
act of revenge which D 504
act as escrow agent D 484
act as carbon sinks D 476
act of drinking water D 458
act of treason under D 428
act of piracy which D 424
act in itself would D 422
act of policy which D 420
act of spending money D 404
act of treason could D 354
act on maxims which D 316
act of choice would D 308
act in school plays D 301
act or policy which D 255
act of murder would D 250
act of applying paint D 238
act of revenge would D 228
act of uttering words D 224
act of thought sight D 222
act of treason would D 204
ace of spades would D 192
act of planting trees D 190
act on infant minds D 178
act in various social D 178
act as trapping sites D 176
act of policy above D 174
act of farther pumping D 168
act as ushers under D 166
act the second scene D 165
act on public trading D 164
act of revenge after D 164
act of another nation D 160
act of shedding blood D 158
act as places where D 157
act as nuclei about D 153
act as levers which D 149
act he caught sight D 146
act of treason within D 144
act of terror which D 142
act as liaison agent D 142
act as target cells D 136
act of painting which D 132
act of assent which D 128
act of piracy would D 126
act as reducing agent D 125
act of policy rather D 124
act of enticing hands D 124
act of cession which D 124
act on cancer cells D 122
act as deputy mayor D 122
ace of spades which D 120
act to another social D 118
act of catching flies D 118
act of treason shall D 116
act of homage would D 116
act as suction pumps D 116
act of terror would D 114
act of revenge rather D 114
act as graham would D 114
act of painting rather D 112
act of itself would D 112
act of positing which D 108
act of painting could D 108
act by shunning action D 108
act of ritual killing D 106
act of coercion would D 106
act of planting seeds D 104
act of incest which D 104
act of wrapping round D 103
act to permit banks D 102
act of inditing tends D 100
act as proton donor D 98
act of speech would D 96
act of shipping goods D 96
act of boyish folly D 96
act by itself would D 96
act of reasoning would D 94
act of policy would D 94
act as settling tanks D 94
act as parish clerk D 94
ace of spades under D 94
act as regent while D 93
act of fetching water D 92
act of emitting bills D 92
act of soviet power D 91
act is already tending D 90
act as deputy prime D 89
act of ocular vision D 88
act of manual skill D 88
act as consul until D 87
act of vomiting which D 86
act of treason might D 86
act of prayer would D 86
act of infamy which D 86
act of rugged mercy D 84
act in various roles D 84
act of theorizing about D 82
act of piracy under D 82
act of memory would D 80
act of bounty which D 80
act by reflex action D 66
act as energy sinks D 53
act as proton pumps D 48
act of whirling round D 47
act in places where D 46
act as nuclei round D 46
act of drinking blood D 44
act as safety valve D 42
