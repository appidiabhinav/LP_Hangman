oil in a skillet D 27525
oil is a mixture D 3816
oil or a mixture D 2560
oil in a shallow D 2128
oil in a blender D 2106
oil as a function D 1568
oil is a natural D 1440
oil as a vehicle D 1384
oil is a complex D 1291
oil as a primary D 974
oil in a mixture D 880
oil is a product D 788
oil in a covered D 593
oil as a solvent D 536
oil in a machine D 531
oil or a similar D 526
oil is a popular D 522
oil at a certain D 502
oil on a regular D 458
oil to a certain D 430
oil of a certain D 424
oil as a carrier D 408
oil is a function D 404
oil is a typical D 394
oil in a certain D 384
oil as a natural D 344
oil in a storage D 330
oil is a mineral D 329
oil by a process D 316
oil or a solution D 300
oil to a skillet D 298
oil on a griddle D 296
oil of a blessed D 280
oil is a problem D 278
oil is a viscous D 256
oil in a plastic D 254
oil in a country D 238
oil to a minimum D 235
oil in a similar D 234
oil of a similar D 220
oil in a furnace D 216
oil as a general D 216
oil of a superior D 214
oil as a coolant D 208
oil is a primary D 206
oil in a variety D 206
oil in a teaspoon D 191
oil of a quality D 190
oil of a peculiar D 190
oil at a reduced D 188
oil as a dietary D 188
oil in a carrier D 170
oil is a similar D 164
oil of a density D 160
oil is a refined D 150
oil is a fungible D 144
oil in a solvent D 144
oil is a national D 140
oil in a tightly D 140
oil to a mixture D 138
oil is a solution D 134
oil in a solution D 134
oil as a reserve D 134
oil in a current D 132
oil as a diluent D 132
oil as a mixture D 130
oil in a process D 128
oil to a mineral D 126
oil at a minimum D 126
oil as a product D 126
oil as a national D 124
oil of a thousand D 122
oil is a perfect D 122
oil is a process D 120
oil is a limited D 120
oil or a mineral D 119
oil is a solvent D 118
oil as a control D 118
oil in a journal D 116
oil at a uniform D 115
oil in a vehicle D 114
oil as a perfume D 114
oil to a recycling D 110
oil at a maximum D 110
oil as a foreign D 108
oil at a premium D 106
oil as a counter D 104
oil to a refiner D 103
oil as a mineral D 103
oil to a storage D 102
oil in a typical D 102
oil in a smaller D 102
oil or a varnish D 100
oil is a general D 100
oil in a viscous D 100
oil in a chamber D 100
oil at a service D 100
oil as a topical D 100
oil on a machine D 96
oil in a limited D 96
oil or a natural D 94
oil on a surface D 94
oil in a deposit D 92
oil as a massage D 92
oil is a familiar D 90
oil is a drastic D 90
oil as a quenching D 90
oil as a residue D 88
oil to a machine D 86
oil in a foreign D 86
oil to a distant D 84
oil is a pleasant D 82
oil is a peculiar D 82
oil is a notable D 80
oil is a neutral D 80
oil in a varnish D 55
oil is a depleting D 46
oil to a maximum D 41
oil in a capsule D 40
