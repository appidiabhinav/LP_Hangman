own doubts and difficulties M 674
own strength and determination M 622
own strength and independence M 606
own wisdom and righteousness M 484
own arrest and imprisonment M 465
own powers and capabilities M 459
own energy and determination M 432
own strength and righteousness M 430
own merits and achievements M 390
own desire for independence M 388
own strength and intelligence M 366
own safety and independence M 356
own bodies and reproductive M 356
own faults and imperfections M 344
own choice and determination M 330
own powers and achievements M 294
own energy and intelligence M 288
own strength and capabilities M 286
own reward and justification M 278
own nature and intelligence M 276
own strength was insufficient M 258
own proper and inaccessible M 250
own errors and imperfections M 225
own honour and independence M 216
own travel and accommodation M 214
own powers are insufficient M 214
own rights and independence M 184
own faults and deficiencies M 168
own vanity any gratification M 166
own training and certification M 144
own senses and consciousness M 144
own mental and intellectual M 144
own without any interference M 142
own dangers and difficulties M 138
own misery and helplessness M 134
own desire and determination M 132
own limits and capabilities M 126
own wishes and capabilities M 122
own design and specification M 122
own pardon and reconciliation M 118
own effort and determination M 116
own spirit and determination M 114
own ethical and intellectual M 114
own actions and achievements M 114
own active and instantaneous M 112
own effort and intelligence M 110
own actions and consequences M 110
own nature and capabilities M 106
own misery and wretchedness M 103
own wisdom and intelligence M 102
own merits and deficiencies M 96
own assets and capabilities M 96
own friend and contemporary M 94
own vanity and inexperience M 92
own values and philosophies M 92
own nature and consciousness M 90
own church has considerable M 90
own senses and intelligence M 86
own forces are insufficient M 86
own thought and consciousness M 84
own nature and consequently M 84
own merits and righteousness M 84
own forces and capabilities M 84
own battle for independence M 82
own safety and tranquillity M 80
own feelings and difficulties M 80
own energy and perseverance D 612
own values and perspectives D 554
own rights and prerogatives D 495
own sexual and reproductive D 468
own skills and capabilities D 460
own tastes and predilections D 412
own safety was incompatible D 406
own powers and prerogatives D 381
own powers and performances D 346
own family and neighborhood D 332
own luxury and extravagance D 324
own doubts and insecurities D 284
own skills and competencies D 252
own tastes and requirements D 234
own income and expenditures D 172
own talent for amplification D 168
own planet are sufficiently D 168
own feelings and relationships D 166
own feelings and recollections D 158
own failings and imperfections D 144
own humors and peculiarities D 142
own unique and irreplaceable D 132
own unique and idiosyncratic D 126
own memory and recollections D 126
own actions and relationships D 126
own feelings and perspectives D 125
own failings and inadequacies D 124
own groups and organizations D 122
own nature too superficially D 118
own feelings and apprehensions D 115
own voices and perspectives D 110
own opinion was controverted D 110
own agents and subordinates D 110
own unique and incomparable D 102
own doubts and perplexities D 102
own wishes and requirements D 100
own skills and achievements D 94
own topics for investigation D 92
own theories and methodologies D 92
own rights and entitlements D 90
own feelings and understandings D 88
own engines and transmissions D 88
own barren and impoverished D 88
own agency and subjectivity D 86
own strength and perseverance D 83
own values and understandings D 82
